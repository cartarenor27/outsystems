pipeline {
 agent any
  parameters {
    // Pipeline parameters are automatically filled by LT Trigger plugin
    string(name: 'ApplicationScope', defaultValue: '', description: 'Comma-separated list of LifeTime applications to deploy.')
    string(name: 'ApplicationScopeWithTests', defaultValue: '', description: 'Comma-separated list of LifeTime applications to deploy (including test applications)')
    string(name: 'TriggeredBy', defaultValue: 'N/A', description: 'Name of LifeTime user that triggered the pipeline remotely.')
  }
  options { skipStagesAfterUnstable() }
  environment {
    // Artifacts Folder
    ArtifactsFolder = "Artifacts"
    // LifeTime Specific Variables
    LifeTimeHostname = 'https://cmiti-lt.outsystemsenterprise.com/'
    LifeTimeAPIVersion = 2
    // Authentication Specific Variables
    AuthorizationToken = credentials('LifeTimeServiceAccountToken')
    // Environments Specification Variables
    /*
    * Pipeline for 5 Environments:
    *   DevelopmentEnvironment    -> Where you develop your applications. This should be the default environment you connect with service studio.
    *   NonProductionEnvironment  -> Where you test your applications.
    *   ProductionEnvironment     -> Where your apps are live.
    */
    DevelopmentEnvironment = 'development environment # 1'
    NonProductionEnvironment = 'non-production environment # 2'
    ProductionEnvironment = 'production environment # 3'
    // Regression URL Specification
    ProbeEnvironmentURL = 'https://cmiti-tst.outsystemsenterprise.com/'
    BddEnvironmentURL = 'https://cmiti-tst.outsystemsenterprise.com/'
    // OutSystems PyPI package version
    OSPackageVersion = '0.6.0'
  }
  stages {
    stage('Install Python Dependencies') {
      steps {
        // Create folder for storing artifacts
        sh script: "mkdir ${env.ArtifactsFolder}", label: 'Create artifacts folder'
        // Only the virtual environment needs to be installed at the system level
        withEnv(["HOME=${env.ArtifactsFolder}"]) {
                sh script: '''
                        #/bin/bash
                        pip install -q -I virtualenv --user
                        export PATH="$ArtifactsFolder/.local/bin:$PATH"
                '''
        // Install the rest of the dependencies at the environment level and not the system level
        withPythonEnv('python3') {
          sh script: '''
                    python3 -m pip install --upgrade pip
                    pip install -U outsystems-pipeline
                    ''' 
          sh script: "pip3 install -U outsystems-pipeline==\"${env.OSPackageVersion}\"", label: 'Install required packages'
          }
        }
      }
    }
    stage('Get and Deploy Latest Tags') {
      steps {
        withEnv(["HOME=${env.ArtifactsFolder}"]) {
        withPythonEnv('python3') {
          echo "Pipeline run triggered remotely by '${params.TriggeredBy}' for the following applications (including tests): '${params.ApplicationScopeWithTests}'"
          // Retrieve the Applications and Environment details from LifeTime
          sh script: "python3 -m outsystems.pipeline.fetch_lifetime_data --artifacts \"${env.ArtifactsFolder}\" --lt_url ${env.LifeTimeHostname} --lt_token ${env.AuthorizationToken} --lt_api_version ${env.LifeTimeAPIVersion}", label: 'Retrieve list of Environments and Applications'
         // Deploy the application list, with tests, to the Regression environment
          lock('deployment-plan-REG') {
            sh script: "python3 -m outsystems.pipeline.deploy_latest_tags_to_target_env --artifacts \"${env.ArtifactsFolder}\" --lt_url ${env.LifeTimeHostname} --lt_token ${env.AuthorizationToken} --lt_api_version ${env.LifeTimeAPIVersion} --source_env \"${env.DevelopmentEnvironment}\" --destination_env \"${env.NonProductionEnvironment}\" --app_list \"${params.ApplicationScopeWithTests}\"", label: "Deploy latest application tags (including tests) to ${env.NonProductionEnvironment}"
            }
          }
        }
      }
    }
    stage('Run Non-Production Environment') {
      when {
        // Checks if there are any test applications in scope before running the regression stage
        expression { return params.ApplicationScope != params.ApplicationScopeWithTests }
      }
            steps {
                withEnv(["HOME=${env.ArtifactsFolder}"]) {
                withPythonEnv('python3') {
                // Generate the URL endpoints of the BDD tests
                sh script: "python3 -m outsystems.pipeline.generate_unit_testing_assembly --artifacts \"${env.ArtifactsFolder}\" --app_list \"${params.ApplicationScopeWithTests}\" --cicd_probe_env ${env.ProbeEnvironmentURL} --bdd_framework_env ${env.BddEnvironmentURL}", label: 'Generate URL endpoints for BDD test suites'
                // Run those tests and generate a JUnit test report
                sh script: "python3 -m outsystems.pipeline.evaluate_test_results --artifacts \"${env.ArtifactsFolder}\"", returnStatus: true, label: 'Run BDD test suites and generate JUnit test report' 
                    }
                }
            }
            post {
                always {
                    withPythonEnv('python3') {
                        echo "Publishing JUnit test results..."
                        junit(testResults: "${env.ArtifactsFolder}\\junit-result.xml", allowEmptyResults: true, skipPublishingChecks: true)
                  }
                }
            }
        }
    stage('Accept Changes') {
      steps {
        // Define milestone before approval gate to manage concurrent builds
        milestone(ordinal: 40, label: 'before-approval')
        // Wrap the confirm option in a timeout to avoid hanging Jenkins forever
        timeout(time:1, unit:'DAYS') {
          input 'Accept changes and deploy to Production?'
        }
        // Discard previous builds that have not been accepted yet
        milestone(ordinal: 50, label: 'after-approval')
      }
    }
    stage('Deploy Dry-Run') {
      steps {
        withEnv(["HOME=${env.ArtifactsFolder}"]) {
        withPythonEnv('python3') {
          // Deploy the application list to the Non-Production environment
          lock('deployment-plan-PRE') {
            sh script: "python3 -m outsystems.pipeline.deploy_latest_tags_to_target_env --artifacts \"${env.ArtifactsFolder}\" --lt_url ${env.LifeTimeHostname} --lt_token ${env.AuthorizationToken} --lt_api_version ${env.LifeTimeAPIVersion} --source_env \"${env.NonProductionEnvironment}\" --destination_env \"${env.ProductionEnvironment}\" --app_list \"${params.ApplicationScope}\" --manifest \"${env.ArtifactsFolder}/deployment_data/deployment_manifest.cache\"", label: "Deploy latest application tags to ${env.ProductionEnvironment}"
          }
        }
      }
    }
  }
    stage('Deploy Production') {
      steps {
        withEnv(["HOME=${env.ArtifactsFolder}"]) {
        withPythonEnv('python3') {
          // Deploy the application list to the Production environment
          lock('deployment-plan-PRD') {
            sh script: "python3 -m outsystems.pipeline.deploy_latest_tags_to_target_env --artifacts \"${env.ArtifactsFolder}\" --lt_url ${env.LifeTimeHostname} --lt_token ${env.AuthorizationToken} --lt_api_version ${env.LifeTimeAPIVersion} --source_env \"${env.NonProductionEnvironment}\" --destination_env \"${env.ProductionEnvironment}\" --app_list \"${params.ApplicationScope}\" --manifest \"${env.ArtifactsFolder}/deployment_data/deployment_manifest.cache\"", label: "Deploy latest application tags to ${env.ProductionEnvironment}"
            }
          }
        }
      }
    }
  }
  post {
    // It will always store the cache files generated, for observability purposes, and notifies the result
    always {
      withEnv(["HOME=${env.ArtifactsFolder}"]) {
      dir ("${env.ArtifactsFolder}") {
        archiveArtifacts artifacts: "**/*.cache"
      }
    }
  }
    // If there's a failure, tries to store the Deployment conflicts (if exists), for troubleshooting purposes
    failure {
      withEnv(["HOME=${env.ArtifactsFolder}"]) {
      dir ("${env.ArtifactsFolder}") {
        archiveArtifacts artifacts: 'DeploymentConflicts', allowEmptyArchive: true
      }
    }
  }
    // Delete artifacts folder content
    cleanup { 
      withEnv(["HOME=${env.ArtifactsFolder}"]) {
      dir ("${env.ArtifactsFolder}") {
        deleteDir()
        }
      }
    }
  }
}
